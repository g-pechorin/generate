package peterlavalle.puregen

import peterlavalle.TemplateResource

object S3ScalaClassesDez extends TemplateResource {

	import S3._

	def apply(module: IR.Module): Stream[String] =
		module.items
			.toStream
			.sortBy(_.name)
			.flatMap {
				case struct: IR.Struct =>
					// yes - we want to do structs
					bind("struct.txt") {
						case "name" => struct.name
						case "dez" => dez(struct.name, struct.args)
						case wild => error("struct unbound " + wild)
					}


				case IR.FSFAtomic(_, _, _) =>
					// atomics will need nothing done
					Nil

				case pipe: IR.Pipe =>
					bind("pipe.txt") {
						case "name" => pipe.name

						case "event" =>
							pipe.events.flatMap {

								case IR.ActionGet(name, Nil) =>
									Stream(".or(" + name + "())")

								case IR.ActionGet(name, args) =>
									".or { " #:: dez(name, args.zipWithIndex.mapr("a" + _).map(_.swap).toList).map("\t" + _) :+ "}"
							}
						case "signal" =>
							pipe.behaviors.flatMap {

								case IR.ActionSet(name, Nil) =>
									Stream(".or(" + name + "())")

								case IR.ActionSet(name, args) =>
									".or { " #:: dez(name, args.zipWithIndex.mapr("a" + _).map(_.swap).toList).map("\t" + _) :+ "}"
							}

						case wild => error("pipe unbound " + wild)
					}

				case IR.Import(name, IR.Module(from, _)) =>
					bind("import.txt") {
						case "from" => from
						case "name" => name
					}

				case IR.Opaque(name) =>
					bind("opaque.txt") {
						case "name" => name
					}

				case wild => error("top level dez - unexpected " + wild)
			}

	private def dez(name: String, args: List[(String, IR.TKind)]): Stream[String] = {
		val a = args
		val n = name
		object struct {
			val name: String = n
			val args: List[(String, IR.TKind)] = a
		}

		bind("txt") {
			case "name" => struct.name
			case "take" =>
				struct.args
					.map {
						case (name, kind) =>
							kind match {
								case (IR.ListOf(_: IR.TAtomicKind)) =>
									s"""$name <- arr("$name", (_: ${struct.name}).$name.map(s => boxValue(s)))"""

								case (IR.ListOf(_)) =>
									s"""$name <- arr("$name", (_: ${struct.name}).$name)"""

								case (_) =>
									s"""$name <- mem("$name", (_: ${struct.name}).$name)"""
							}
					}
			case "args" =>
				struct
					.args
					.map {
						case (name, _) =>
							name + " <- " + name
					}
			case "pass" =>
				struct.args
					.map {
						case (name, kind) =>
							kind match {
								case IR.ListOf(_: IR.TAtomicKind) =>
									name + ".toStream.map(_.v)"
								case IR.ListOf(_: IR.Struct) =>
									name + ".toStream"
								case _ => name
							}
					}
			case wild =>
				error(s"fill in wild = `$wild`")
		}
	}
}
