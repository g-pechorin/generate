package peterlavalle.puregen

import peterlavalle.io.ReadSource
import peterlavalle.{DepCir, E, TemplateResource}

import scala.io.{BufferedSource, Source}

object PureIn extends TemplateResource {

	private val ext = ".pidl"

	def parse(src: ReadSource): Stream[(String, IR.Module)] =
		DepCir
			.tossCircle(src.list) {
				(done: String => IR.Module) =>
					(name: String) =>
						Source.fromInputStream(src.open(name))
							.using {
								src: BufferedSource =>
									assume(name.endsWith(ext))
									PCG(
										(want: String) => done(want + ext),
										name.dropRight(ext.length).replace('/', '.'),
										src.mkString
									) match {
										case E.Success(v) =>
											v
									}
							}
			}
			.toStream
}
