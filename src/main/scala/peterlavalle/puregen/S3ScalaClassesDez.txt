for {
	<@take/>
} yield {
	for {
		<@args/>
	} yield {
		<@name/>(
			<@pass/>,
		)
	}
}
