package peterlavalle.puregen

import java.io.{File, Writer}

import peterlavalle.io.{OverWriter, ReadSource}
import peterlavalle.{TLogFile, TemplateResource}

/**
 * base trait that others extend
 *
 * no teplztes itself tho
 */
trait S3 extends TemplateResource with TLogFile {

	override protected def logFile: TLogFile.LogFile = S3.logFile

	/**
	 *
	 * @param pack ???
	 * @param src  a list of source folders to scan for files we compile
	 * @param inc  a list of source folders to scan for files we can "lookup"
	 * @param into
	 * @return
	 */
	final def apply(pack: String, src: Seq[File], inc: Seq[File], into: File): List[File] = {

		log.out(
			s"""
				 |pack = $pack
				 |into ${into.AbsolutePath}
			 """.stripMargin
		)

		lazy val see =
			src.map(ReadSource.fromDir(_: File).exclusively((_: String).endsWith(".pidl"))).reduce((_: ReadSource) combine (_: ReadSource))

		lazy val all: ReadSource =
			inc
				.filter((_: File).exists())
				.map(ReadSource(_: File).exclusively((_: String).endsWith(".pidl")))
				.foldLeft(see)((_: ReadSource) combine (_: ReadSource))

		val modules: Set[IR.Module] =
			PureIn.parse(all)
				.filter((self: (String, IR.Module)) => see.list.contains(self._1))
				.toMap
				.values
				.toSet

		OverWriter(into) {
			into: OverWriter =>

				// write new files
				translate(pack, modules)
					.map {
						case (path, data) =>

							log.out("writing " + path)

							data
								.foldLeft(into.writer(path): Writer) {
									(_: Writer).append(_: String).append("\n")
								}.close()
							path
					}.toSet

					// convert them to a list
					.toList.sorted.map(into / (_: String))
		}
	}

	def translate(pack: String, modules: Set[IR.Module]): Map[String, Stream[String]]

	def ++(them: S3): S3 = {
		(pack: String, modules: Set[IR.Module]) => {
			val self: Map[String, Stream[String]] = translate(pack, modules)
			val next: Map[String, Stream[String]] = them.translate(pack, modules)
			// ensure no name collisions
			require(!self.keySet.exists(next.keySet))
			self ++ next
		}
	}
}

object S3 {
	private lazy val logFile: TLogFile.LogFile = {
		val file: File = "target" / "S3.log"
		TLogFile.FileLog(file)
	}

	implicit class PrimStrings(strings: Seq[String]) {
		def collapse(c: String): String = strings.commas(c).foldLeft("")(_ + _)

		def commas(c: String = ","): Stream[String] =
			strings.mapIsLast {
				case (text, true) => text
				case (text, false) => text + c
			}
	}

	implicit class PrimpPipe(pipe: IR.Pipe) {
		def events: Stream[IR.ActionGet] = pipe.actions.filterAs[IR.ActionGet].toStream.sortBy((_: IR.ActionGet).name)

		def behaviors: Stream[IR.ActionSet] = pipe.actions.filterAs[IR.ActionSet].toStream.sortBy((_: IR.ActionSet).name)

		def structs: Stream[IR.Struct] = (pipe.args ++ pipe.actions).structs
	}

	implicit class PrimModule(module: IR.Module) {
		def definitions: List[IR.TDefinition] = module.items.toList.sortBy(_.name)
	}

	implicit class PrimListIRIR(irs: Iterable[IR.IR]) {

		/**
		 * does/should crawl any/all constructs to find any/all structures we've defined
		 */
		def structs: Stream[IR.Struct] =
			irs
				.flatMap {
					(_: IR.IR).$recurStreamDistinct() {
						case action: IR.TAction =>
							action.args

						case IR.ListOf(data) =>
							List(data)

						case IR.Struct(_, args) =>
							args.map((_: (String, IR.TKind))._2)

						case
							// i don;t think that we need one
							_: IR.Import |


							_: IR.TAtomicKind | _: IR.Opaque =>
							// TODO;not fully covered
							Nil

						case missing =>
							error(
								"need to extract structures from " + missing.getClass
							)
					}
				}
				.toStream
				.distinct.filterAs[IR.Struct]
				.toStream
				.sortBy((_: IR.Struct).name)

	}

	object Scala extends S3 {

		implicit class PiTKind(ir: IR.TKind) {
			def toPureScriptFromScala: String =
				ir match {
					case IR.ListOf(kind) =>
						".map((v: " + kind.toScala + ") => v" + kind.toPureScriptFromScala + ").toArray"

					case IR.Bool => ": java.lang.Boolean"
					case IR.Real32 => ": java.lang.Float"
					case IR.Real64 => ": java.lang.Double"
					case IR.Text => ": java.lang.String"

					case IR.Struct(_, _) => ": Value"

					case IR.SInt64 =>
						// TODO; cover this in test(s)
						": java.lang.Long"

					case what =>
						error(
							"don't know how to convert this: " + what.getClass
						)
				}

			def fromPureScript(left: String): String = {
				val of: IR.TKind => String = {
					case IR.Struct(struct, _) => struct
					case IR.Real32 => "Float"
					case IR.Real64 => "Double"
					case IR.SInt32 => "Int"
					case IR.Bool => "Boolean"

					case what =>
						error(
							"don't know how to convert this: " + what.getClass
						)
				}

				ir match {
					case imported: IR.Import =>
						imported.actual.fromPureScript(left)
					case atomic: IR.TAtomicKind => s"$left.toValue[" + of(atomic) + "]"
					case IR.Struct(struct, _) => s"$left.toValue[$struct]"
					case IR.ListOf(what) => s"$left.toValue[Stream[" + of(what) + "]]"
					case IR.Opaque(name) =>
						// TODO; not covered by UNIT test
						s"$left.as[$name](classOf[$name])"
				}
			}

			def toScala: String =
				ir match {
					case IR.Import(name, from) => s"${from.name}.$name"
					case IR.Bool => "Boolean"

					case IR.Real32 => "Float"
					case IR.Real64 => "Double"

					case IR.SInt32 => "Int"
					case IR.SInt64 => "Long"

					case IR.Text => "String"

					case IR.ListOf(kind) => "Stream[" + kind.toScala + "]"

					case IR.Opaque(name) => name
					case IR.Struct(name, _) => name
				}

			def toField: String = {
				def of: IR.TKind => String = {
					case IR.Bool => "Boolean"
					case IR.Real32 => "Float"
					case IR.Real64 => "Double"
					case IR.Text => "String"
					case IR.Struct(name, _) => name
					case IR.SInt64 =>
						error("// TODO; cover this in test(s)")
						"Long"

					case what =>
						error(
							"failed to convert thing " + what.getClass
						)

				}

				ir match {
					case IR.ListOf(kind) => "stream[" + of(kind) + "]"
					case kind => "field[" + of(kind) + "]"
				}
			}

			/**
			 * prepare data to pass from Scala into ECMAScript
			 */
			def toPass(name: String): String =
				ir match {
					case IR.Struct(_, _) =>
						name + ": Value"

					case IR.ListOf(what) =>
						name + ".map((v: " + what.toScala + ") => " + what.toPass("v") + ").toArray"

					case IR.Text =>
						// TODO cover it with test
						name + ": String"

					case IR.Bool =>
						name + ": java.lang.Boolean"

					case unexpected =>
						error(
							"the pureGenerator doesn't know how to pass " + unexpected.name + " from Scala into PureScript"
						)
				}
		}

		final val ScalaClasses: S3 = S3ScalaClasses
		/**
		 * generates a trait that includes all modules
		 *
		 * ... could be better ... maybe
		 */
		val IndexClasses = S3IndexClasses

		override def translate(pack: String, modules: Set[IR.Module]): Map[String, Stream[String]] = {
			(ScalaClasses ++ IndexClasses) translate(pack, modules)
		}
	}

	object Script extends S3 {
		/**
		 * the pure-script-js files
		 */
		val JavaScript: S3 = S3JavaScript

		/**
		 * the pure-script-purs files
		 */
		val PureScript: S3 = S3PureScript

		override def translate(pack: String, modules: Set[IR.Module]): Map[String, Stream[String]] = {
			(JavaScript ++ PureScript) translate(pack, modules)
		}
	}

}
