
implicit lazy val dez<@name/>Event: DeRecord[<@name/>.Event] = {
	import <@name/>._
	alt[<@name/>.Event] {
		(_)
			<@event/>
	}
}

implicit lazy val dez<@name/>Signal: DeRecord[<@name/>.Signal] = {
	import <@name/>._
	alt[<@name/>.Signal] {
		(_)
			<@signal/>
	}
}
