package peterlavalle

import java.io._
import java.util.Date

import peterlavalle.TLogFile.LogFile

/**
 * trait. you mix it in and you get prettier logs
 */
trait TLogFile {
	protected def logFile: LogFile

	protected lazy val code: String = getClass.getName

	final object log {
		final def out(message: Any): Unit = {
			val text =
				message.toString.split("[\r \t]*\n")
					.toList
					.map(code + " ; " + (_: String))
					.foldLeft("")((_: String) + (_: String) + "\n")

			logFile.send((_: Writer).write(text))
		}

		final def err(message: Any): Unit = {
			val text =
				message.toString
					.split("[\r \t]*\n")
					.toList
					.map(code + " ! " + (_: String))
					.foldLeft("")((_: String) + (_: String) + "\n")

			logFile.send((_: Writer).write(text))
		}
	}


	final def println(x: Any): Unit = {
		log.out(x)
		println(x)
	}

	final def error(message: Any): Nothing =
		try {
			peterlavalle.error(message)
		} catch {
			case e: Throwable =>
				// pull our frame off of the stack
				e.setStackTrace(e.getStackTrace.tail)

				// send the data to the log file(s)
				val writer = new StringWriter()
				e.printStackTrace(new PrintWriter(writer))
				log.err(writer.toString)

				// throw the exception
				throw e
		}

}

object TLogFile {

	trait LogFile {

		def send(f: Writer => Unit): Unit

	}

	case class FileLog(into: File) extends LogFile {


		private lazy val fileWriter = {
			if (!into.isFile) {
				System.err.println("no file - no log ;;; " + into.AbsolutePath)
				null
			} else {
				System.err.println("log in ;;; " + into.AbsolutePath)
				new FileWriter(into.EnsureParent, true)
					.append(new Date().iso8061Long + "\n")
			}
		}

		override def send(f: Writer => Unit): Unit = {
			if (null != fileWriter)
				synchronized {
					f(fileWriter)
					fileWriter.flush()
				}
		}
	}

}
